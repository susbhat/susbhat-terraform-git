provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "terraform-vm-rg"
  location = "West Europe"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "terraform-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "terra-subnet1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "wpip" {
  name                    = "terra-wpip"
  location                = azurerm_resource_group.rg.location
  resource_group_name     = azurerm_resource_group.rg.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
}

resource "azurerm_public_ip" "lpip" {
  name                    = "terra-lpip"
  location                = azurerm_resource_group.rg.location
  resource_group_name     = azurerm_resource_group.rg.name
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
}


resource "azurerm_network_interface" "wnic" {
  name                = "terra-wnic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

    ip_configuration {
    name                          = "terra-wip"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.wpip.id
  }

}

resource "azurerm_network_interface" "lnic" {
  name                = "terra-lnic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

   ip_configuration {
    name                          = "terra-lip"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.lpip.id
  }
}


resource "azurerm_windows_virtual_machine" "winvm" {
  name                = "tf-win-vm"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = "Standard_B2s"
  admin_username      = "susbhat"
  admin_password      = "Welcome@12345"
  network_interface_ids = [
    azurerm_network_interface.wnic.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
provisioner "local-exec" {
    command = "echo ${azurerm_public_ip.wpip.ip_address} >> wprivate_ips.txt"
}

//    provisioner "remote_exec" {
//      inline = [
//          "choco install graphviz"
//      ]
//  }

}

resource "azurerm_linux_virtual_machine" "linuxvm" {
  name                            = "tf-linux-vm"
  resource_group_name             = azurerm_resource_group.rg.name
  location                        = azurerm_resource_group.rg.location
  size                            = "Standard_B2s"
  admin_username                  = "adminuser"
  admin_password                  = "Welcome@12345"
  disable_password_authentication = "false"
  network_interface_ids = [
    azurerm_network_interface.lnic.id,
  ]


  //   admin_ssh_key {
  //     username   = "adminuser"
  //     public_key = file("${path.module}/id_rsa.pub")
  //   }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  provisioner "local-exec" {
    command = "echo ${azurerm_public_ip.lpip.ip_address} >> lprivate_ips.txt"
  }

    // provisioner "remote_exec" {
    //   inline = [
    //       "sudo yum -y install nano"
    //   ]
    // }
}

output "win_public_ip_address" {
  value = azurerm_public_ip.wpip.ip_address
}

output "lin_public_ip_address" {
  value = azurerm_public_ip.lpip.ip_address
}


// azurerm_public_ip.lpip.id


